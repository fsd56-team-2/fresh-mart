import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrl: './fruits.component.css'
})
export class FruitsComponent implements OnInit {
 
  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  constructor(private service : ServicesService) {

    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id : 201,
        name:'Apple',
        description: 'Apple are considered nutrient-dense fruits, contains Fiber,Vitamin C,Copper,Vit K,Pottassium,Carbs.',
        expiry:'6-Feb-2024',
        price: 145.00 ,
        imageUrl: 'https://thumbs.dreamstime.com/b/beautiful-apples-brown-wooden-background-46849192.jpg'
      },
      {
        id : 202,
        name:' Banana ',
        description: 'Bananas are a rich source of potassium, fiber, vitamin C, and other nutrients that can support various aspects of your health, such as energy, digestion, heart, bone. ',
        expiry:'27-Feb-2024',
        price:69.00,
        imageUrl: 'https://th.bing.com/th/id/OIP.FTeCTyTZMjbRE77twGY9rgHaEK?rs=1&pid=ImgDetMain '
      },
      {
        id : 203,
        name:'Orange',
        description:  'Oranges are rich in vitamin C, calcium, potassium, folate, phosphorus, and beta-carotene.  ',
        expiry:'7-Mar-2024',
        price: 49.00,
        imageUrl: 'https://www.wallpaperflare.com/static/568/497/158/orange-citrus-fruit-fruit-healthy-wallpaper.jpg'
      },
      {
        id : 204,
        name:' Guava',
        description: ' Guava is a tropical fruit rich in vitamin C, fiber, potassium and antioxidants. It can boost the immune system, regulate blood pressure, lower blood sugar levels, fight cancer and more ',
        expiry:'7-Mar-2024',
        price: 499.00,
        imageUrl: 'https://wallpapercave.com/wp/wp3120118.jpg '
      },
      {
        id : 205,
        name:'Pine Apple ',
        description: ' Pine Apple is good for hair, skin, and bones. It contains vitamin A, vitamin K, phosphorus, calcium, and zinc.   ',
        expiry:'9-Mar-2024',
        price: 199.00,
        imageUrl: 'https://nisargafoods.com/image/cache/catalog/Fruits/Pineapple-800x800.jpg'
      },
      {
        id : 206,
        name:'Black Grapes ',
        description: 'Black Grapes are packed with vitamin C, a powerful antioxidant that plays key roles in immune system health, connective tissue development, and wound healing.',
        expiry:'10-Mar-2024',
        price: 160.00,
        imageUrl: 'https://wallpapercave.com/wp/wp3405645.jpg'
      },
      {
        id : 207,
        name:'Green Grapes ',
        description: 'Green Grapes are good at vitamin C, a powerful antioxidant that plays key roles in immune system health, connective tissue development, and wound healing.',
        expiry:'6-March-2024',
        price: 120.00,
        imageUrl: 'https://wallpapercave.com/wp/BssWhNH.jpg'
      },
      {
        id : 208,
        name:'Straw Berry ',
        description: 'Strawberry functions to boost brainpower. This tempting fruit is rich in iodine, phytochemicals, and Vitamin C and it contains potassium too. ',
        expiry:'10-March-2024',
        price: 250.00,
        imageUrl: 'https://images5.alphacoders.com/431/thumb-1920-431647.jpg '
      },
    ];
  }

  ngOnInit() {
  }
  addToCart(product:any){
    this.service.addToCart(product);
  }
}