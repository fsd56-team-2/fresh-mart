import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';

@Component({
  selector: 'app-pulses',
  templateUrl: './pulses.component.html',
  styleUrl: './pulses.component.css'
})
export class PulsesComponent implements OnInit{

  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  constructor(private service : ServicesService) {

    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id : 401,
        name:'Bengal Gram',
        description: 'Bengal Gramis a very good source of calories,Protein,Carbohydrates.',
        expiry:'27-Apr-2024',
        price: 299.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.AfqlhHtaXiGf6BAEqHaB9gHaFj?pid=ImgDet&w=206&h=154&c=7&dpr=1.3'
       },
       {
        id : 401,
        name:'Green Gram',
        description: 'Green gram is high in fibre,low in calories ,contains proteins and iron.',
        expiry:'27-Apr-2024',
        price: 299.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.sK1Fv_DoEyj8iHSVi2i__QHaEs?rs=1&pid=ImgDetMain'
       },
       {
        id : 401,
        name:'Finger Millet ( Ragi)',
        description: 'Ragi(Finger Millets) comprises a vast array of key nutrients like vitamin C, vitamin E, B-complex vitamins, iron, calcium, antioxidants, proteins, fibers, sufficient calories and useful unsaturated fats.',
        expiry:'27-Apr-2024',
        price: 399.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.pMobSqa4wVYXM5f1VQgo8QHaFg?w=282&h=210&c=7&r=0&o=5&dpr=1.3&pid=1.7'
       },
       {
        id : 401,
        name:'Cashew Nuts',
        description: 'Cashew Nuts are Heart-Healthy,helps in lowering Blood Pressure and improves bone Health',
        expiry:'27-Apr-2024',
        price: 345.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.MS3rIBALonpTUdPkPDcr3gHaE8?w=1198&h=800&rs=1&pid=ImgDetMain'
       },
       {
        id : 401,
        name:'Brown Mustard Seeds',
        description: 'Mustard seeds contain similar constituents, including about 30 to 40 percent vegetable oil, a slightly smaller proportion of protein, and myrosin.',
        expiry:'27-Apr-2024',
        price: 199.00 ,
        imageUrl: 'https://i.ebayimg.com/images/g/j74AAOSw5uNfnYYO/s-l300.jpg'
       },
       {
        id : 401,
        name:'Maize Grains',
        description: 'Maize is rich in riboflavin, phosphorus, potash, iron, calcium, zinc, and vitamin B.',
        expiry:'27-Apr-2024',
        price: 199.00 ,
        imageUrl: 'https://image.freepik.com/free-photo/raw-corn-seed_95419-2229.jpg'
       },
      
    ];
  }

  ngOnInit() {
  }
  addToCart(product:any){
    this.service.addToCart(product);
  }

}