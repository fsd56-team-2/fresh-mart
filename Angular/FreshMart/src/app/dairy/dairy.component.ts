import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';

@Component({
  selector: 'app-dairy',
  templateUrl: './dairy.component.html',
  styleUrl: './dairy.component.css'
})
export class DairyComponent implements OnInit{

  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  constructor(private service : ServicesService) {

    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id : 301,
        name:'Milk',
        description: ' Milk is a very good source of calcium, phosphorus, vitamin D, riboflavin, and vitamin B12.',
        expiry:'26-Feb-2024',
        price: 99.00 ,
        imageUrl: 'assets/images/Milk.jpg'
      },
      {
        id : 302,
        name:' Ghee ',
        description: 'Ghee is also an excellent source of Vitamin E and rich source of vitamins, antioxidants, and healthy fats.',
        expiry:'27-Mar-2024',
        price:640.00,
        imageUrl: 'assets/images/Ghee.jpg '
      },
      {
        id : 303,
        name:'Panner',
        description: 'Paneer is a good source of selenium,calcium,phosphorous,carbohydrates. ',
        expiry:'27-Feb-2024',
        price: 440.00,
        imageUrl: 'assets/images/Panner.jpg '
      },
    ];
  }

  ngOnInit() {
  }
  addToCart(product:any){
    this.service.addToCart(product);
  }

}
