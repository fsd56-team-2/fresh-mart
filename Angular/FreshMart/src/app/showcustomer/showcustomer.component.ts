import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';


declare var jQuery: any;
@Component({
  selector: 'app-showcustomer',
  templateUrl: './showcustomer.component.html',
  styleUrl: './showcustomer.component.css'
})
export class ShowcustomerComponent implements OnInit {
  customers: any;
  emailId: any;
  countries: any;
  editCustomers:any;

  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');

    this.editCustomers = {
      customerId: '',
      customerName: '',
      emailId: '',
      phoneNumber: '',
      gender: '',
    };
  }

  ngOnInit() {
    this.service.getAllCustomers().subscribe((data: any) => {
      this.customers = data.filter((customer: { emailId: any; }) => customer.emailId == this.emailId);
    });
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
  }

  editCustomer(customer:any){
    console.log(customer);
    this.editCustomers = customer;
    
    jQuery('#myModal').modal('show');
  }
  

 
}


