import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Subject } from 'rxjs/internal/Subject';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
 
 


 
  loginstatus: any;
  cartItems: any;
 
  isUserLoggedIn: boolean;
  loginStatus:any;

  constructor(private http:HttpClient) {
    this.cartItems=[];
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }

  addToCart(product: any) {
    this.cartItems.push(product);
  }
  getCartItems(): any {
    return this.cartItems;
  }
  setCartItems() {
    this.cartItems.splice();
  }

   //Login
   setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }  

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  clearCartItems() {
    this.cartItems = [];
  }
  



  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  getAllCountries(){
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  // Customer services


  updateCustomer(employee: any) {
    return this.http.put('http://localhost:8080/updateCustomer', employee);
  }
  deleteCustomer(empId: any) {
    return this.http.delete('http://localhost:8080/deleteCustomerById/' + empId);
  }


  getAllCustomers(){
    return this.http.get('http://localhost:8080/getCustomers');
  }

  customerLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8080/customerLogin/' + emailId + '/' + password).toPromise();
  }

  addCustomer(customer: any): any {
    return this.http.post('http://localhost:8080/addCustomer', customer);
  }

  getEmailId(emailId : any):any{
    return this.http.get('http://localhost:8080/getEmailId/' + emailId).toPromise();
  }

  fetchOtp(otp : any) :any{
    return this.http.get('http://localhost:8080/fetchOtp/' + otp).toPromise();
  }

  updatePassword(password : any , otp : any):any{
    return this.http.put('http://localhost:8080/updatePassword/'+otp, password).toPromise();
  }
  
}