import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent implements OnInit {
  loginStatus: any;
  cartItems: any;

  constructor(private service: ServicesService,private router:Router) {
    this.cartItems = service.getCartItems();
  }
  



  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
  navigateTodairy() : void{
    this.router.navigateByUrl('/dairy');
  }
  
  navigateTofruits():void{
    this.router.navigateByUrl('/fruits');
  }
  
  navigateTopulses():void{
    this.router.navigateByUrl('/pulses');
  }

  navigateTovegetables():void{
    this.router.navigateByUrl('/vegetables');
  }
  

  navigateTocustomerLogin(): void {
    this.router.navigateByUrl('/login'); // Assuming 'login' is the route path for your login component
  }

  navigateToshowcustomer(): void{
    this.router.navigateByUrl('/showcustomer');
  }

  }



