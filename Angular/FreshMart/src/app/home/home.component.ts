import { Component } from '@angular/core';
import { ServicesService } from '../service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  
  products:any[];
  
  constructor(private service: ServicesService) {
    this.products = [
      {
        id : 201,
        name:'Apple',
        description: 'Apple are considered nutrient-dense fruits, contains Fiber,Vitamin C,Copper,Vit K,Pottassium,Carbs.',
        expiry:'6-Feb-2024',
        price: 145.00 ,
        imageUrl: 'https://thumbs.dreamstime.com/b/beautiful-apples-brown-wooden-background-46849192.jpg'
      },
      {
        id : 202,
        name:' Banana ',
        description: 'Bananas are a rich source of potassium, fiber, vitamin C, and other nutrients such as energy, digestion, heart, bone. ',
        expiry:'27-Feb-2024',
        price:69.00,
        imageUrl: 'https://th.bing.com/th/id/OIP.FTeCTyTZMjbRE77twGY9rgHaEK?rs=1&pid=ImgDetMain '
      },
      {
        id : 203,
        name:'Orange',
        description:  'Oranges are rich in vitamin C, calcium, potassium, folate, phosphorus, and beta-carotene.  ',
        expiry:'7-Mar-2024',
        price: 49.00,
        imageUrl: 'https://www.wallpaperflare.com/static/568/497/158/orange-citrus-fruit-fruit-healthy-wallpaper.jpg'
      },
      {
        id : 201,
        name:'Bottle Gourd',
        description: ' It offers vitamin C and a moderate amount of fiber, bottle gourd is a rich source of phytonutrients.',
        expiry:'27-Feb-2024',
        price: 45.00 ,
        imageUrl: 'assets/images/Bottleguard.jpg'
      },
      {
        id : 202,
        name:' Brinjal ',
        description: 'Brinjal is a good source of dietary fiber, which can help promote healthy digestion and prevent constipation  ',
        expiry:'27-Feb-2024',
        price:40.00,
        imageUrl: 'assets/images/Brinjal.jpg '
      },
      {
        id : 203,
        name:'Ladies Finger ',
        description: 'Ladies finger is a rich source of Vitamin C and K1. As Vitamin C is a water-soluble nutrient,it boosts overall immune functioning.   ',
        expiry:'26-Feb-2024',
        price: 40.00,
        imageUrl: 'assets/images/Ladiesfinger.jpg '
      },
      {
        id : 204,
        name:' Red Mirchi ',
        description: ' Red mirchi is a rich source of vitamin C, which is a potential antioxidant. ',
        expiry:'25-April-2024',
        price: 400.00,
        imageUrl: 'assets/images/Redmirchi.jpg '
      },
      {
        id : 205,
        name:'Green Beans  ',
        description: 'The fiber in green beans helps to keep your digestive system healthy and running smoothly.  ',
        expiry:'25-Feb-2024',
        price: 60.00,
        imageUrl: 'assets/images/Green Beans.jpg '
      },
      {
        id : 206,
        name:'Green Mirchi  ',
        description: 'Green chilli is a rich source of vitamin C,which is a potential antioxidant and is responsible for maintaining the elasticity of the skin',
        expiry:'26-Feb-2024',
        price: 60.00,
        imageUrl: 'assets/images/Green Mirchi.jpg '
      },
      {
        id : 207,
        name:'Ginger  ',
        description: ' Ginger is widely used as a spice and a folk medicine.',
        expiry:'27-Mar-2024',
        price: 200.00,
        imageUrl: 'assets/images/Ginger.jpg '
      },
      {
        id : 208,
        name:'Carrot ',
        description: ' Carrots also provide a good range of nutritional benefits for very few calories ',
        expiry:'28-Feb-2024',
        price: 50.00,
        imageUrl: 'assets/images/Carrot.jpg '
      },
      {
        id : 204,
        name:' Guava',
        description: ' Guava is a tropical fruit rich in vitamin C, fiber, potassium and antioxidants. It can boost the immune system, regulate blood pressure. ',
        expiry:'7-Mar-2024',
        price: 499.00,
        imageUrl: 'https://wallpapercave.com/wp/wp3120118.jpg '
      },
      {
        id : 205,
        name:'Pine Apple ',
        description: ' Pine Apple is good for hair, skin, and bones. It contains vitamin A, vitamin K, phosphorus, calcium, and zinc.   ',
        expiry:'9-Mar-2024',
        price: 199.00,
        imageUrl: 'https://nisargafoods.com/image/cache/catalog/Fruits/Pineapple-800x800.jpg'
      },
      {
        id : 206,
        name:'Black Grapes ',
        description: 'Black Grapes consists of vitamin C,a powerful antioxidant that plays key roles in immune system health, connective tissue development.',
        expiry:'10-Mar-2024',
        price: 160.00,
        imageUrl: 'https://wallpapercave.com/wp/wp3405645.jpg'
      },
      {
        id : 207,
        name:'Green Grapes ',
        description: 'Green Grapes are good at vitamin C, a powerful antioxidant that plays key roles in immune system health, connective tissue development,.',
        expiry:'6-March-2024',
        price: 120.00,
        imageUrl: 'https://wallpapercave.com/wp/BssWhNH.jpg'
      },
      {
        id : 208,
        name:'Straw Berry ',
        description: 'Strawberry functions to boost brainpower. This tempting fruit is rich in iodine, phytochemicals, and Vitamin C and it contains potassium too. ',
        expiry:'10-March-2024',
        price: 250.00,
        imageUrl: 'https://images5.alphacoders.com/431/thumb-1920-431647.jpg '
      },
     
      { name: 'Ginger', description: 'Ginger is widely used as a spice and a folk medicine.', price: 200.00, imageUrl: './assets/images/Ginger.jpg' },
      { name: 'Carrot ', description: 'Carrots also provide a good range of nutritional benefits for very few calories', price: 50.00, imageUrl: './assets/images/Carrot.jpg' },
      { name: 'Milk', description: 'Milk is a very good source of calcium, phosphorus, vitamin D, riboflavin, and vitamin B12.', price: 45.00, imageUrl: './assets/images/Milk.jpg' },
      { name: 'Panner', description: 'Paneer is a good source of selenium,calcium,phosphorous,carbohydrates.', price: 40.00, imageUrl: './assets/images/Panner.jpg' },
      { name: 'Spinach', description: 'It is a rich source of many micronutrients like vitamin A, vitamin C, calcium, magnesium, beta-carotene and folic acid.', price: 47.00, imageUrl: './assets/images/Spinach.jpg' },
      { name: 'Honey', description: ' Honey has no protein, fat and fiber. It contains a number of antioxidants including phenolic compounds like flavonoids. ', price: 1000, imageUrl: './assets/images/Honey.jpg' },
      { name: 'RedMirchi', description: 'Red mirchi is a rich source of vitamin C, which is a potential antioxidant ', price: 400.00, imageUrl: './assets/images/Redmirchi.jpg' },
      {
        id : 401,
        name:'Bengal Gram',
        description: 'Bengal Gramis a very good source of calories,Protein,Carbohydrates.',
        expiry:'27-Apr-2024',
        price: 299.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.AfqlhHtaXiGf6BAEqHaB9gHaFj?pid=ImgDet&w=206&h=154&c=7&dpr=1.3'
       },
       {
        id : 401,
        name:'Green Gram',
        description: 'Green gram is high in fibre,low in calories ,contains proteins and iron.',
        expiry:'27-Apr-2024',
        price: 299.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.sK1Fv_DoEyj8iHSVi2i__QHaEs?rs=1&pid=ImgDetMain'
       },
       {
        id : 401,
        name:'Finger Millet ( Ragi)',
        description: 'Ragi(Finger Millets) comprises a vast array of key nutrients like vitamin C, vitamin E, B-complex vitamins, iron, calcium, antioxidants.',
        expiry:'27-Apr-2024',
        price: 399.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.pMobSqa4wVYXM5f1VQgo8QHaFg?w=282&h=210&c=7&r=0&o=5&dpr=1.3&pid=1.7'
       },
       {
        id : 401,
        name:'Cashew Nuts',
        description: 'Cashew Nuts are Heart-Healthy,helps in lowering Blood Pressure and improves bone Health',
        expiry:'27-Apr-2024',
        price: 345.00 ,
        imageUrl: 'https://th.bing.com/th/id/OIP.MS3rIBALonpTUdPkPDcr3gHaE8?w=1198&h=800&rs=1&pid=ImgDetMain'
       },
       {
        id : 401,
        name:'Brown Mustard Seeds',
        description: 'Mustard seeds contain similar constituents, including about 30 to 40 percent vegetable oil, a slightly smaller proportion of protein, and myrosin.',
        expiry:'27-Apr-2024',
        price: 199.00 ,
        imageUrl: 'https://i.ebayimg.com/images/g/j74AAOSw5uNfnYYO/s-l300.jpg'
       },
       {
        id : 401,
        name:'Maize Grains',
        description: 'Maize is rich in riboflavin, phosphorus, potash, iron, calcium, zinc, and vitamin B.',
        expiry:'27-Apr-2024',
        price: 199.00 ,
        imageUrl: 'https://image.freepik.com/free-photo/raw-corn-seed_95419-2229.jpg'
       },
      
    ]
  }

}