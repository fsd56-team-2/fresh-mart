import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { CustomerotpverificationComponent } from './customerotpverification/customerotpverification.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { CustomerupdatepasswordComponent } from './customerupdatepassword/customerupdatepassword.component';
import { CartComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { FruitsComponent } from './fruits/fruits.component';
import { DairyComponent } from './dairy/dairy.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { PulsesComponent } from './pulses/pulses.component';
import { ShowcustomerComponent } from './showcustomer/showcustomer.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AdminComponent } from './admin/admin.component';




const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'login',       component:LoginComponent},
  {path:'logout',      component:LogoutComponent},
  {path:'register',    component:RegisterComponent},
  {path :'aboutus' , component:AboutusComponent},
  {path :'customerotpverification' , component:CustomerotpverificationComponent },
  {path :'customerupdatepassword' , component:CustomerupdatepasswordComponent},
  {path:'customerforgotpassword', component:ForgotpasswordComponent},
  {path :'cart' ,canActivate:[authGuard], component:CartComponent},
  {path :'fruits' , canActivate:[authGuard],component:FruitsComponent},
  {path :'dairy' , canActivate:[authGuard],component:DairyComponent},
  {path :'vegetables' ,canActivate:[authGuard], component:VegetablesComponent},
  {path :'pulses' ,canActivate:[authGuard], component:PulsesComponent},
  {path :'products' , canActivate:[authGuard],component:ProductsComponent},
  {path:'showcustomer',canActivate:[authGuard],component:ShowcustomerComponent},
  { path:'checkout/:total', canActivate:[authGuard], component: CheckoutComponent },
  {path:'adminhome',component:AdminComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
