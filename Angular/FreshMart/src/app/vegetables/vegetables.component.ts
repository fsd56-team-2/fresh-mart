import { Component } from '@angular/core';
import { ServicesService } from '../service.service';

@Component({
  selector: 'app-vegetables',
  templateUrl: './vegetables.component.html',
  styleUrl: './vegetables.component.css'
})
export class VegetablesComponent {

  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  constructor(private service : ServicesService) {

    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id : 201,
        name:'Bottle Gourd',
        description: ' It offers vitamin C and a moderate amount of fiber, bottle gourd is a rich source of phytonutrients.',
        expiry:'27-Feb-2024',
        price: 45.00 ,
        imageUrl: 'assets/images/Bottleguard.jpg'
      },
      {
        id : 202,
        name:' Brinjal ',
        description: 'Brinjal is a good source of dietary fiber, which can help promote healthy digestion and prevent constipation  ',
        expiry:'27-Feb-2024',
        price:40.00,
        imageUrl: 'assets/images/Brinjal.jpg '
      },
      {
        id : 203,
        name:'Ladies Finger ',
        description: 'Ladies finger is a rich source of Vitamin C and K1. As Vitamin C is a water-soluble nutrient, it boosts the overall immune functioning.   ',
        expiry:'26-Feb-2024',
        price: 40.00,
        imageUrl: 'assets/images/Ladiesfinger.jpg '
      },
      {
        id : 204,
        name:' Red Mirchi ',
        description: ' Red mirchi is a rich source of vitamin C, which is a potential antioxidant and is responsible for maintaining the elasticity of the skin ',
        expiry:'25-April-2024',
        price: 400.00,
        imageUrl: 'assets/images/Redmirchi.jpg '
      },
      {
        id : 205,
        name:'Green Beans  ',
        description: 'The fiber in green beans helps to keep your digestive system healthy and running smoothly.  ',
        expiry:'25-Feb-2024',
        price: 60.00,
        imageUrl: 'assets/images/Green Beans.jpg '
      },
      {
        id : 206,
        name:'Green Mirchi  ',
        description: 'Green chilli is a rich source of vitamin C, which is a potential antioxidant and is responsible for maintaining the elasticity of the skin',
        expiry:'26-Feb-2024',
        price: 60.00,
        imageUrl: 'assets/images/Green Mirchi.jpg '
      },
      {
        id : 207,
        name:'Ginger  ',
        description: ' Ginger is widely used as a spice and a folk medicine.',
        expiry:'27-Mar-2024',
        price: 200.00,
        imageUrl: 'assets/images/Ginger.jpg '
      },
      {
        id : 208,
        name:'Carrot ',
        description: ' Carrots also provide a good range of nutritional benefits for very few calories ',
        expiry:'28-Feb-2024',
        price: 50.00,
        imageUrl: 'assets/images/Carrot.jpg '
      },
    ];
  }

  ngOnInit() {
  }
  addToCart(product:any){
    this.service.addToCart(product);
  }

}