

import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {
  localStorageData: any;
  emailId: any;
  total: number = 0;
  products: any;

  constructor(private service: ServicesService, private router: Router) {
    this.products = service.getCartItems();
    this.calculateTotal();
  }

  ngOnInit() {
  }

  deleteCartProduct(product: any) {
    const index = this.products.findIndex((p: any) => p.id === product.id);
    if (index !== -1) {
      this.total -= (product.price * product.quantity);
      this.products.splice(index, 1);
    }
  }

  updateQuantity(product: any, quantity: number) {
    const index = this.products.findIndex((p: any) => p.id === product.id);
    if (index !== -1) {
      this.total -= (product.price * product.quantity);
      this.products[index].quantity = quantity;
      this.total += (product.price * quantity);
    }
  }

  purchase() {
    this.products = [];
    this.service.clearCartItems();
    this.router.navigate(['checkout', this.total]);
  }

  private calculateTotal() {
    this.total = this.products.reduce((acc: number, product: any) => {
      return acc + (product.price * product.quantity);
    }, 0);
  }
}
