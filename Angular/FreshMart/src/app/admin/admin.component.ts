import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../service.service';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css'
})
export class AdminComponent implements OnInit{

  customers: any;
  emailId: any;
  showCustomerTableFlag: boolean = false;

  constructor(private service : ServicesService){
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllCustomers().subscribe((data:any) => { this.customers = data });
  }

  showCustomerTable() {
    this.service.getAllCustomers
    this.showCustomerTableFlag = true;
  
  }

}
