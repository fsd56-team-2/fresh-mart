import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsComponent } from './products/products.component';
import { RouterModule } from '@angular/router';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { AboutusComponent } from './aboutus/aboutus.component';
import { CartComponent } from './cart/cart.component';
import { CustomerotpverificationComponent } from './customerotpverification/customerotpverification.component';
import { CustomerupdatepasswordComponent } from './customerupdatepassword/customerupdatepassword.component';
import { FooterComponent } from './footer/footer.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HomeComponent } from './home/home.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { DairyComponent } from './dairy/dairy.component';
import { PulsesComponent } from './pulses/pulses.component';
import { FruitsComponent } from './fruits/fruits.component';
import { ShowcustomerComponent } from './showcustomer/showcustomer.component';
import { GenderPipe } from './gender.pipe';
import { CheckoutComponent } from './checkout/checkout.component';
import { AdminComponent } from './admin/admin.component';







@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProductsComponent,
    LogoutComponent,
    RegisterComponent,
    NavbarComponent,
    AboutusComponent,
    CartComponent,
    CustomerotpverificationComponent,
    CustomerupdatepasswordComponent,
    FooterComponent,
    ForgotpasswordComponent,
    HomeComponent,
    AboutusComponent,
    VegetablesComponent,
    DairyComponent,
    PulsesComponent,
    FruitsComponent,
    ShowcustomerComponent,
    GenderPipe,
    CheckoutComponent,
    AdminComponent,


  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    ToastrModule.forRoot(),
    NgxCaptchaModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule

    

  ],
  providers: [
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
